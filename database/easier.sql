CREATE USER easieruser IDENTIFIED BY 'YOURDBPASSWORD';
DROP DATABASE IF EXISTS easier;
CREATE DATABASE IF NOT EXISTS easier;

USE easier;

CREATE TABLE user (
 id VARCHAR(50) NOT NULL,
 pkname  VARCHAR(50),
 pktype VARCHAR(50), 
 pksize INT, 
 pk BLOB, 
 mkname VARCHAR(50),
 mktype VARCHAR(50), 
 mksize INT, 
 mk BLOB,
 pxkname VARCHAR(50),
 pxktype VARCHAR(50), 
 pxksize INT, 
 pxk BLOB, 
 attr VARCHAR(500)
) ENGINE=INNODB;

CREATE TABLE link (
 id1 VARCHAR(50) NOT NULL,
 id2 VARCHAR(50) NOT NULL,
 skname VARCHAR(50),
 sktype VARCHAR(50),
 sksize INT,
 sk BLOB,
 attr VARCHAR(500),
 revoked TINYINT(1),
 id2uk VARCHAR(100),
 delegate TINYINT(1) NOT NULL
) ENGINE=INNODB;

CREATE TABLE fbuser (
 id VARCHAR(50) NOT NULL,
 name VARCHAR (100) NOT NULL,
 isappuser TINYINT,
 about VARCHAR(500),
 gender VARCHAR(10) 
) ENGINE=INNODB;

CREATE TABLE fbfriend (
 id1 VARCHAR(50) NOT NULL,
 id2 VARCHAR(50) NOT NULL
) ENGINE=INNODB;

CREATE TABLE encdata (
 id VARCHAR(50) NOT NULL,
 enccpabename VARCHAR(50),
 enccpabetype VARCHAR(50), 
 enccpabesize INT, 
 enccpabe BLOB,
 enccpaesname VARCHAR(50),
 enccpaestype VARCHAR(50), 
 enccpaessize INT, 
 enccpaes BLOB
) ENGINE=INNODB;

CREATE TABLE delkey (
 id1 VARCHAR(50) NOT NULL,
 id2 VARCHAR(50) NOT NULL,
 id3 VARCHAR(50) NOT NULL,
 skdelname VARCHAR(50) NOT NULL,
 skdel BLOB NOT NULL
) ENGINE=INNODB;

