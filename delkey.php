<?php
require_once 'facebook-php-sdk/src/facebook.php';
require_once 'dbappinclude.php';

session_start();

$facebook = null;
$me = null;
$msg = null;
$delinfomsg = null;

/* HOME button pressed */
if (isset($_POST['home_button'])) {
	header("Location: index.php");
}

/* delegate pressed from index.php */
if (isset($_POST['delKey'])) {

  	$facebook = unserialize($_SESSION['fb']);
	if ($facebook->getUser()) {
		try {
			$me = $facebook->api('/me');
		} 
		catch (FacebookApiException $e) {
			error_log($e);
		}
	}
}

 /*delegate pressed from this form */
 if (isset($_POST['del_key'])) {
	
  	$facebook = unserialize($_SESSION['fb']);

	if ($facebook->getUser()) {
		try {
		  $me = $facebook->api('/me');
		  delKey();
		} 
		catch (FacebookApiException $e) {
			error_log($e);
		}
	}
}

/* read pk and mk from database */
function getPKMK($myuid) {

	$sqlstr = "SELECT pkname, pktype, pksize, pk, mkname, mktype, mksize, mk FROM user WHERE id='$myuid'";
	$result = query($sqlstr);

	list($pkname, $pktype, $pksize, $pkcontent, $mkname, $mktype, $mksize, $mkcontent) = mysql_fetch_array($result);

	if ($pkcontent && $mkcontent) {

		$fhandle = fopen($pkname, "w");
		fwrite($fhandle, $pkcontent);
		fclose($fhandle);

		$fhandle = fopen($mkname, "w");
		fwrite($fhandle, $mkcontent);
		fclose($fhandle);

		return true;
	} 
	else {
		return false;
	}
}

/* delegate fof attribute from received keys that have fof attribute */
function delKey() {

	global $facebook;
	global $me;	
	global $msg;
	global $delinfomsg;

	if ($me) {
		if (isset($_POST['del_key'])) {

			$myuid = $me['id'];

			$rcvdkeys = null;
			if (isset($_POST['rcvdkeys_checkbox'])) {
				$rcvdkeys = $_POST['rcvdkeys_checkbox'];
			}
			  
			$cnames = null;
			if (isset($_POST['cnames_checkbox'])) {
				$cnames = $_POST['cnames_checkbox'];
			}
			  
			getPKMK($myuid);
		
			for($i=0; $i < sizeof($rcvdkeys) ; $i++) {
				$skname = "sk-$rcvdkeys[$i]-$myuid";
				getSKToDel($skname);
				//echo "rcvd: ". $rcvdkeys[$i] . "<br>";
				//sleep(1);
				for($j=0; $j < sizeof($cnames) ; $j++) 
				{
					//echo "cname: " . $cnames[$j] . "<br>";
					if($rcvdkeys[$i] == $cnames[$j])
						continue;
					$keyexists = delkeyExists("sk-del-" . $rcvdkeys[$i] . "-" . $myuid . "-" . $cnames[$j]);
					if( $keyexists > 0)
					{
						$delinfomsg = $delinfomsg . "sk-del-" . $rcvdkeys[$i] . "-" . $myuid . "-" . $cnames[$j] . "<br>";
						continue;
					}
	
					$cuk = trim(getUK($cnames[$j]));
					$skdelname = "sk-del-$rcvdkeys[$i]-$myuid-$cnames[$j]";
					$keygen_cmd = "cpabe-delegate -o $skdelname pk-$myuid mk-$myuid $skname cpabe-users-del-$myuid $cuk fof";	
						
					exec($keygen_cmd);
					//echo $keygen_cmd . "<br>";
					
					// add to the database;
					$fhandle = fopen($skdelname, 'r');
					$skdelsize = filesize($skdelname);
					$skdeltype = filetype($skdelname);
					$skdelcontent = fread($fhandle, $skdelsize);
					$skdelcontent = addslashes($skdelcontent);
						
					$sqlstr = "INSERT INTO delkey (id1,id2,id3,skdelname,skdel) VALUES('$rcvdkeys[$i]', '$myuid', '$cnames[$j]', '$skdelname', '$skdelcontent')";
					query($sqlstr);
					//echo $sqlstr . "<br><br>";
		  			if(file_exists($skdelname))	unlink($skdelname);
				}
				unlink("pk-" . $myuid);
		  		unlink("mk-" . $myuid);
		  		unlink($skname);
		  		unlink($cpname);
		  		unlink("cpabe-users-$myuid");
		  		unlink("cpabe-users-del-$myuid");
			}
		  
			if ($rcvdkeys == null) {
				$msg = "No Keys to Delegate";
				return false;
			}
			if ($cnames == null) {
				$msg = "No Contacts to Delegate";
				return false;
			}
            }
	 }
}

function showDelKeys() {
	global $facebook;
	global $me;

	if ($me) {
		$myuid = $me['id'];

		$sqlstr = "SELECT fbuser.name, link.id2, attr, revoked, id2uk FROM fbuser,link WHERE link.id1=$myuid AND link.id2=fbuser.id";
		$result = query($sqlstr);

		//get the contacts that I gave a key
		while (list($cname, $cid, $cattr, $crevoked, $cuk) = mysql_fetch_array($result)) {
			echo "<tr> <td>$cname </td> <td> $cattr </td> <td> $crevoked  </td></tr>";
		}
	}
}

function delkeyExists($skdelname) {
	global $facebook;
	global $me;

	if ($me) {
		$myuid = $me['id'];

		$sqlstr = "SELECT COUNT(*) FROM delkey WHERE skdelname LIKE '$skdelname'";
		$result = query($sqlstr);
		
		list($count) = mysql_fetch_array($result);
		return $count;
	}
}

// get uk of user uid	
function getUK($uid)
{
	global $me;
	  
	if ($me) {
		$myuid = $me['id'];
		  
		$sqlstr = "SELECT id2uk FROM link WHERE id2 LIKE '$uid'";
		$result = query($sqlstr);
		  
		list($id2uk) = mysql_fetch_array($result);

		return $id2uk;
	}	
}

// returns my SK which are allowed to delegate, i.e. contain fof attribute
function getSKToDel($skname)
{
	if(file_exists($skname)) {
		//echo $skname . "exists" . "<br>";
		return true;
	}
	
	$sqlstr = "SELECT sk FROM link WHERE skname LIKE '$skname'";
	$result = query($sqlstr);
	//echo $skname . "----------------------------------";
	if($result)
	{
		list($skcontent) = mysql_fetch_array($result);
		  
		$fhandle = fopen($skname, "w");
		fwrite($fhandle, $skcontent);
		fclose($fhandle);
	  
		return true;
	}
	return false;
}

/* show the users that gave me a key */
function showReceivedKeys() {
	global $facebook;
	global $me;

	if ($me) {
		$myuid = $me['id'];

		$sqlstr = "SELECT fbuser.name, link.id1 FROM fbuser,link WHERE link.id2='$myuid' AND link.id1=fbuser.id AND delegate=1";
		$result = query($sqlstr);
		$idlist = array();
	  
		while (list($cname, $cid) = mysql_fetch_array($result)) {
			$idlist[$cid] = $cname;	 	
			echo "<INPUT TYPE=CHECKBOX NAME=\"rcvdkeys_checkbox[]\" VALUE=\"$cid\" >$idlist[$cid]<BR>";	
		}
	}
}
  //show the users that I sent keys
  function showSentKeys() {
	  global $facebook;
	  global $me;
	  global $delto;

	  if ($me) {
	  $myuid = $me['id'];

	  $sqlstr = "SELECT fbuser.name, link.id2 FROM fbuser,link WHERE link.id1=$myuid AND link.id2=fbuser.id AND revoked=0";
	  $result = query($sqlstr);

	  //get the contacts that I gave a key
	  while (list($cname, $cid) = mysql_fetch_array($result)) {
		  echo "<INPUT TYPE=CHECKBOX NAME=\"cnames_checkbox[]\" VALUE=\"$cid\" >$cname<BR>";
			  //$delto["$cid"] = $cuk;
			  //echo $delto["$cid"] . "<br>";
	  }
	  }
  }
  ?>

  <html xmlns:fb="http://www.facebook.com/2008/fbml">
	  <head>
	  <title>DelKey</title>
	  </head>
	  <body>
	  <h3>Delegate Secret Keys</h3>

	  <form method="post" enctype="multipart/form-data" action="delkey.php">
		  <table width="700" border="1" cellpadding="1" cellspacing="1" class="box">
		  <tr>
			  <td>
			  <b>Check Received Keys </b><br>
			  <?php showReceivedKeys(); ?>
			  </td>
			  <td>
			  <b>Check Contacts </b><br>
			  <?php showSentKeys(); ?>
			  </td>
		  </tr>
		  <tr>
			<td><input name="del_key" type="submit" class="box" id="del_key" value=" Delegate Key ">
			  <?php global $msg;
			  echo $msg; ?>
			</td>
		  </tr>
		  <tr>
			<td> <input name="home_button" type="submit" class="box" id="home_button" value=" Home "> </td>
		  </tr>

		<tr>
			<td> 
			<?php 	global $delinfomsg; 
				if($delinfomsg) 
					echo "Ignored, already delegated : <br>" . $delinfomsg ; 
			?>
			</td>
		</tr>
		</table>
	  </form>

	  </body>
  </html>
