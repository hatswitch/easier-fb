<?php
require_once 'facebook-php-sdk/src/facebook.php';
require_once 'dbappinclude.php';

$facebook = null;
$me = null;
$msg = null;
//from index.php
if (isset($_POST['enc'])) {

    session_start();
    $facebook = unserialize($_SESSION['fb']);

    //if ($facebook->getSession()) {
    if ($facebook->getUser()) {
        try {
            $me = $facebook->api('/me');
        } catch (FacebookApiException $e) {
            error_log($e);
        }
    }
}
//from this page
if (isset($_POST['enc_button'])) {

    session_start();
    $facebook = unserialize($_SESSION['fb']);

    //if ($facebook->getSession()) {
    if ($facebook->getUser()) {
        try {
            $me = $facebook->api('/me');
            encrypt();
        } catch (FacebookApiException $e) {
            error_log($e);
        }
    }
}

if (isset($_POST['home_button'])) {
   header("Location: index.php");
}

function getPK($myuid) {
    $sqlstr = "SELECT pkname, pktype, pksize, pk FROM user WHERE id='$myuid'";
    $result = query($sqlstr);

    list($pkname, $pktype, $pksize, $pkcontent) = mysql_fetch_array($result);

    //read pk and mk from database, generate key, save the keys, and delete pk, mk from disk
    if ($pkcontent) {

        $fhandle = fopen($pkname, "w");
        fwrite($fhandle, $pkcontent);
        fclose($fhandle);
    }
}

function encrypt() {
    global $facebook;
    global $me;
    global $msg;

    if ($me) {
        $myuid = $me['id'];

        $profile_combo = null;
        if (isset($_POST['profile'])) {
            $profile_combo = $_POST['profile'];
        }

        $policy_text = null;
        if (isset($_POST['policy'])) {
            $policy_text = $_POST['policy'];
        }

        if(!$profile_combo || !$policy_text)
        {
            $msg = "Invalid Data/Policy!";
            return false;
        }
        $msg = null;

        $fname = date('ymdHis');
        $fhandle = fopen($fname, "w");
        fwrite($fhandle, $profile_combo);
        fclose($fhandle);

	//echo ' ======= ' . $profile_combo . ' ======= <br> <br>';


        getPK($myuid);

        $enc_cmd = "cpabe-enc pk-$myuid $fname '$policy_text'";

        //echo $enc_cmd;
        exec($enc_cmd);

        $encname = $fname . '.cpabe';

        $fp = fopen($encname, 'r');
        $encsize = filesize($encname);
        $enctype = filetype($encname);
        $enccontent = fread($fp, $encsize);
        $enccontent = addslashes($enccontent);
        fclose($fp);

        if (!get_magic_quotes_gpc()) {
            $encname = addslashes($encname);
        }

        $enc_cpaes_name = $fname . '.cpaes';
        
        $fp = fopen($enc_cpaes_name, 'r');
        $enc_cpaes_size = filesize($enc_cpaes_name);
        $enc_cpaes_type = filetype($enc_cpaes_name);
        $enc_cpaes_content = fread($fp, $enc_cpaes_size);
        $enc_cpaes_content = addslashes($enc_cpaes_content);
        fclose($fp);
        
        if (!get_magic_quotes_gpc()) {
            $enc_cpaes_name = addslashes($enc_cpaes_name);
        }
        
        $sqlstr = "INSERT INTO encdata (id, enccpabe, enccpabename, enccpabesize, enccpabetype, enccpaes, enccpaesname, enccpaessize, enccpaestype) "
                . "VALUES ('$myuid', '$enccontent', '$encname', '$encsize', '$enctype', '$enc_cpaes_content', '$enc_cpaes_name', '$enc_cpaes_size', '$enc_cpaes_type')";

        query($sqlstr);

        unlink("pk-$myuid");
        unlink($encname);
        unlink($enc_cpaes_name);
    }
}

function showAttr() {

    global $facebook;
    global $me;

    if ($me) {
        $myuid = $me['id'];

        $sqlstr = "SELECT attr FROM user where id='$myuid'";
        $result = query($sqlstr);

        list($attrs) = mysql_fetch_array($result);

        if (strlen($attrs) > 0) {
            $attrlist = explode(" ", $attrs);

            $len = sizeof($attrlist);
            if ($len > 0) {

                echo '<ul>';
                for ($i = 0; $i < $len; $i++) {
                    echo "<li>$attrlist[$i]";
                }
                echo '</ul>';
            }
        }
    }
}

function showProfile() {
    global $facebook;
    global $me;

    if ($me) {
        $myuid = $me['id'];

        $sqlstr = "SELECT about,gender FROM fbuser where id='$myuid'";
        $result = query($sqlstr);

        if (mysql_num_rows($result) > 0) {
            list($about, $gender) = mysql_fetch_array($result);

            echo "<option value=\"" . $myuid . "\"> id : $myuid </option>";
            echo "<option value=\"" . $about . "\"> about : $about </option>";
            echo "<option value=\"" . $gender . "\"> gender : $gender </option>";
        }
    }
}

function showEncFiles() {

    global $me;

    $myuid = $me['id'];

    $query = "SELECT enccpabename FROM encdata WHERE id='$myuid'";

    $result = query($query);


    while (list($name) = mysql_fetch_array($result)) {
?>
        <tr>
            <!--<td> <a href="download.php?filename=<?php //echo $name; ?>&type=3"> <?php //echo $name ?> </a></td> -->
	    <td> <?php echo $name; ?> </td>
        </tr>
<?php
    }
}
?>



<html xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <title>Encrypt</title>
    </head>
    <body>
        <form method="post" enctype="multipart/form-data" action="enc.php">
            <table width="700" border="1" cellpadding="1" cellspacing="1" class="box">
                <tr>
                    <td>
                        <b>Select a data: </b><br>
                        <select name="profile">
<?php showProfile(); ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Your defined attributes: </b><br>
<?php showAttr(); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Create a policy using your attributes (e.g., friend or family and 1 of (coworker,neighbor)): </b><br>
                        <input name="policy" type="text">
                    </td>
                </tr>
                <tr>
                    <td>
                        <input name="enc_button" type="submit" class="box" id="enc_button" value=" Encrypt ">
                        <?php echo $msg;?>
                    </td>
                </tr>
            </table>
	    <b>Files You Have Encrypted:</b>
	    <table width="700" border="1" >
	      <?php showEncFiles(); ?>
	      <tr>
		<td> <input name="home_button" type="submit" class="box" id="home_button" value=" Home "> </td>
	      </tr>
	    </table>
        </form>
    </body>
</html>
