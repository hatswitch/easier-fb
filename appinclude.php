<?php
require_once 'facebook-php-sdk/src/facebook.php';

$facebook = new Facebook(array(
            'appId' => 'APP_ID_THAT_YOU_GET_FROM_FACEBOOK_WHILE_CREATING_EASIER',
            'secret' => 'APP_SECRET_THAT_YOU_GET_FROM_FACEBOOK_WHILE_CREATING_EASIER',
            'cookie' => true,
        ));

session_start();
$_SESSION['fb'] = serialize($facebook);
$_SESSION['index'] = 'index.php';
