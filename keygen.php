<?php
require_once 'facebook-php-sdk/src/facebook.php';
require_once 'dbappinclude.php';

$facebook = null;
$me = null;
$msg = null;

if (isset($_POST['home_button'])) {
   header("Location: index.php");
}

//from index.php
if (isset($_POST['keyGen'])) {

    session_start();
    $facebook = unserialize($_SESSION['fb']);

    //if ($facebook->getSession()) {
    if ($facebook->getUser()) {
        try {
            $me = $facebook->api('/me');
        } catch (FacebookApiException $e) {
            error_log($e);
        }
    }
}

//from this form
if (isset($_POST['asgn_key'])) {

    session_start();
    $facebook = unserialize($_SESSION['fb']);

    //if ($facebook->getSession()) {
    if ($facebook->getUser()) {
        try {
            $me = $facebook->api('/me');
            keyGen();
        } catch (FacebookApiException $e) {
            error_log($e);
        }
    }
}

function getPKMK($myuid) {

    $sqlstr = "SELECT pkname, pktype, pksize, pk, mkname, mktype, mksize, mk FROM user WHERE id='$myuid'";
    $result = query($sqlstr);

    list($pkname, $pktype, $pksize, $pkcontent, $mkname, $mktype, $mksize, $mkcontent) = mysql_fetch_array($result);

    //read pk and mk from database
    if ($pkcontent && $mkcontent) {

        $fhandle = fopen($pkname, "w");
        fwrite($fhandle, $pkcontent);
        fclose($fhandle);

        $fhandle = fopen($mkname, "w");
        fwrite($fhandle, $mkcontent);
        fclose($fhandle);

        return true;
    } else {
        return false;
    }
}

function keyGen() {

    global $facebook;
    global $me;
    global $msg;

    if ($me) {

        if (isset($_POST['asgn_key'])) {

	    $delegate = 0;
	    
            $myuid = $me['id'];

            $app_user_id_combo = null;
            if (isset($_POST['app_user_combo'])) {
                $app_user_id_combo = $_POST['app_user_combo'];
            }

            $keygen_cmd = "cpabe-keygen -o sk-$myuid-$app_user_id_combo pk-$myuid mk-$myuid cpabe-users-$myuid cpabe-id-$myuid-$app_user_id_combo $app_user_id_combo ";

            if (isset($_POST['attrs_checkbox'])) {
                $attrs_checkbox = $_POST['attrs_checkbox'];
            }
            $asgned_attrs = null;
            while (list ($key, $val) = @each($attrs_checkbox)) {
                $keygen_cmd = $keygen_cmd . " $val";
                $asgned_attrs = $asgned_attrs . "$val ";
		if(strcmp($val, 'fof') == 0)
		{
		  $delegate = 1;
		}
            }

            if ($asgned_attrs == null || $app_user_id_combo == null) {
                $msg = "No User or Attribute Selected";
                return false;
            }

            $msg = null;
            $ret = getPKMK($myuid);

            //generate secret key, save the keys, and delete pk, mk from disk
            if ($ret == true) {

                //echo $keygen_cmd . "<br>";
                exec($keygen_cmd);

                $skname = 'sk-' . $myuid . '-' . $app_user_id_combo;
                $fhandle = fopen($skname, 'r');
                $sksize = filesize($skname);
                $sktype = filetype($skname);
                $skcontent = fread($fhandle, $sksize);
                $skcontent = trim(addslashes($skcontent));
                fclose($fhandle);

                //$cpname = "cpabe-users-$myuid";
                $cpname = "cpabe-id-$myuid-$app_user_id_combo";
                $fhandle = fopen($cpname, 'r');
                $cpsize = filesize($cpname);
                $cptype = filetype($cpname);
                $cpcontent = fread($fhandle, $cpsize);
                $cpcontent = trim(addslashes($cpcontent));

                fclose($fhandle);

                if (!get_magic_quotes_gpc()) {

                    $skname = addslashes($skname);
                }

                $asgned_attrs = trim($asgned_attrs);

                $sqlstr = "INSERT INTO link (id1,id2,sk,sksize,sktype,skname,attr,revoked,id2uk,delegate) 
			   VALUES ('$myuid', '$app_user_id_combo', '$skcontent', '$sksize', '$sktype', '$skname', '$asgned_attrs', '0', '$cpcontent', '$delegate')";

		//echo $sqlstr . "<br>";
                query($sqlstr);

                unlink("pk-" . $myuid);
                unlink("mk-" . $myuid);
                unlink($skname);
                unlink($cpname);
                unlink("cpabe-users-$myuid");
            }
        }
    }
}

function showAttr() {

    global $facebook;
    global $me;

    if ($me) {
        $myuid = $me['id'];

        $sqlstr = "SELECT attr FROM user where id='$myuid'";
        $result = query($sqlstr);

        list($attrs) = mysql_fetch_array($result);

        if (strlen($attrs) > 0) {
            $attrlist = explode(" ", $attrs);

            $len = sizeof($attrlist);
            if ($len > 0) {

                for ($i = 0; $i < $len; $i++) {
                    echo "<INPUT TYPE=CHECKBOX NAME=\"attrs_checkbox[]\" VALUE=\"$attrlist[$i]\" >$attrlist[$i]<BR>";
                }
            }
        }
    }
}

function showAppFBFriends() {
    global $facebook;
    global $me;

    if ($me) {
        $myuid = $me['id'];

        //$sqlstr = "SELECT id2 FROM fbfriend WHERE id1='$myuid'
        //            AND id2 IN (SELECT id FROM fbuser WHERE isappuser=1)"; //select FB friends who use this app
        $sqlstr = "SELECT id,name FROM fbuser WHERE isappuser='1' AND id!=$myuid AND id NOT IN (SELECT id2 FROM link WHERE id1='$myuid')"; //select people who use this app
        $result = query($sqlstr);

        while (list($app_fbfriends_id, $app_fbfriends_name) = mysql_fetch_array($result)) {
            echo "<option value=\"" . $app_fbfriends_id . "\" name=> $app_fbfriends_name </option>";
        }
    }
}

function showSentKeys() {
    global $facebook;
    global $me;

    if ($me) {
        $myuid = $me['id'];

        $sqlstr = "SELECT fbuser.name, link.id2, attr, revoked, id2uk FROM fbuser,link WHERE link.id1=$myuid AND link.id2=fbuser.id";
        $result = query($sqlstr);

        //get the contactsc that I gave a key
        while (list($cname, $cid, $cattr, $crevoked, $cuk) = mysql_fetch_array($result)) {
            echo "<tr> <td>$cname </td> <td> $cattr </td> <td> $crevoked  </td></tr>";
        }
    }
}

function showReceivedKeys() {
    global $facebook;
    global $me;

    if ($me) {
        $myuid = $me['id'];

        $sqlstr = "SELECT fbuser.name, link.id1, link.sksize, link.sktype, link.skname, link.sk FROM fbuser,link
            WHERE link.id2='$myuid' AND link.id1=fbuser.id";
        $result = query($sqlstr);

        //get the contacts that I gave a key
        while (list($cname, $cid, $csksize, $csktype, $cskname) = mysql_fetch_array($result)) {
            echo "<tr> <td>$cname </td> <td> <a href=\"download.php?filename=<?php echo $cskname; ?>&type=2\"> Secret Key </a> </td></tr>";
        }
    }
}
?>

<html xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <title>KeyGen</title>
    </head>
    <body>
        <h3>Manage Secret Keys for Contacts</h3>

        <form method="post" enctype="multipart/form-data" action="keygen.php">
            <table width="700" border="1" cellpadding="1" cellspacing="1" class="box">
                <tr>
                    <td>
                        <b>Select a contact: </b><br>
                        <select name="app_user_combo">
                            <?php showAppFBFriends(); ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Check attributes: </b><br>
                        <?php showAttr(); ?>

                        </td>
                    </tr>
                    <tr>
                        <td><input name="asgn_key" type="submit" class="box" id="asgn_key" value=" Assign Key ">
                        <?php global $msg;
                            echo $msg; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>My assigned keys:</b> <br>
                            <table width ="700" border="1">

                                <tr>
                                    <td>
                                        <b> To</b>
                                    </td>
                                    <td>
                                        <b>Attributes</b>
                                    </td>
                                    <td>
                                        <b>Revoked </b>
                                    </td>
                                </tr>

<?php showSentKeys(); ?>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>My received keys:</b> <br>
                        <table width ="700" border="1">

                            <tr>
                                <td>
                                    <b> From </b>
                                </td>
                                <td>
                                    <b>Key</b>
                                </td>

                            </tr>
<?php showReceivedKeys(); ?>
                        </table>

                    </td>
                </tr>
		<tr>
		  <td> <input name="home_button" type="submit" class="box" id="home_button" value=" Home "> </td>
		</tr>
            </table>

        </form>

    </body>
</html>
