<?php
require_once 'facebook-php-sdk/src/facebook.php';
require_once 'dbappinclude.php';

if (isset($_POST['upload']) && $_FILES['pkfile']['size'] > 0 && $_FILES['mkfile']['size'] > 0) {
    $pkname = $_FILES['pkfile']['name'];
    $pktmpname = $_FILES['pkfile']['tmp_name'];
    $pksize = $_FILES['pkfile']['size'];
    $pktype = $_FILES['pkfile']['type'];

    $fp = fopen($pktmpname, 'r');
    $pkcontent = fread($fp, filesize($pktmpname));
    $pkcontent = addslashes($pkcontent);
    fclose($fp);

    $mkname = $_FILES['mkfile']['name'];
    $mktmpname = $_FILES['mkfile']['tmp_name'];
    $mksize = $_FILES['mkfile']['size'];
    $mktype = $_FILES['mkfile']['type'];

    $fp = fopen($mktmpname, 'r');
    $mkcontent = fread($fp, filesize($mktmpname));
    $mkcontent = addslashes($mkcontent);
    fclose($fp);

    if (!get_magic_quotes_gpc()) {
        $pkname = addslashes($pkname);
        $mkname = addslashes($mkname);
    }

    session_start();
    $facebook = unserialize($_SESSION['fb']);

    if ($facebook->getSession()) {
        try {
            $me = $facebook->api('/me');
            $uid = $me['id'];

            $query = "INSERT INTO easier.user (id, pk, pkname, pksize, pktype, mk, mkname, mksize, mktype ) "
            . "VALUES ('$uid', '$pkcontent', '$pkname', '$pksize', '$pktype', '$mkcontent', '$mkname', '$mksize', '$mktype')";

            query($query) or die('Error, query failed');

            echo "$pkName and  $mkName uploaded successfully" . "<br>";

            $_SESSION['setup'] = true;
            header('Location: '. $_SESSION['homepage']);
          
        } catch (FacebookApiException $e) {
            error_log($e);
        }
    }
}
?>