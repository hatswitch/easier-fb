<?php

require_once 'facebook-php-sdk/src/facebook.php';
require_once 'dbappinclude.php';

$facebook = unserialize($_SESSION['fb']);

if ($facebook->getSession()) {
    try {

        $me = $facebook->api('/me');
        $uid = $me['id'];

        $pkname = 'pk-' . $uid;
        $pksize = filesize($pkname);
        $pktype = filetype($pkname);

        $fp = fopen($pkname, 'r');
        $pkcontent = fread($fp, $pksize);
        $pkcontent = addslashes($pkcontent);
        fclose($fp);

        $mkname = 'mk-'.$uid;
        $mksize = filesize($mkname);
        $mktype = filetype($mkname);

        $fp = fopen($mkname, 'r');
        $mkcontent = fread($fp, $mksize);
        $mkcontent = addslashes($mkcontent);
        fclose($fp);

        if (!get_magic_quotes_gpc()) {
            $pkname = addslashes($pkname);
            $mkname = addslashes($mkname);
        }

        $query = "INSERT INTO easier.user (id, pk, pkname, pksize, pktype, mk, mkname, mksize, mktype ) "
                . "VALUES ('$uid', '$pkcontent', '$pkname', '$pksize', '$pktype', '$mkcontent', '$mkname', '$mksize', '$mktype')";

        query($query);

        echo "$pkName and  $mkName uploaded successfully" . "<br>";

        unlink($pkname);
        unlink($mkname);

        $_SESSION['setup'] = true;
        header('Location: ' . $_SESSION['homepage']);
    } catch (FacebookApiException $e) {
        error_log($e);
    }
}
?>
