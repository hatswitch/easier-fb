<?php
require_once 'facebook-php-sdk/src/facebook.php';
require_once 'dbappinclude.php';

session_start();

/* Home button pressed */
if (isset($_POST['home_button'])) {
   header("Location: index.php");
}

$facebook = unserialize($_SESSION['fb']);
$me = $facebook->api('/me');
$myuid = $me['id'];

$cid = null;
$cname = null;

/* get my public_key*/
function getPK($id) 
{
    $sqlstr = "SELECT pkname, pktype, pksize, pk FROM user WHERE id='$id'";
    $result = query($sqlstr);

    list($pkname, $pktype, $pksize, $pkcontent) = mysql_fetch_array($result);

    if ($pkcontent) 
    {
        $fhandle = fopen($pkname, "w");
        fwrite($fhandle, $pkcontent);
        fclose($fhandle);
        return $pkname;
    }
    return false;
}

/* get encrypted file */
function getEnc($senderid, $fname) 
{
    $sqlstr = "SELECT enccpabename, enccpabetype, enccpabesize, enccpabe, enccpaesname, enccpaestype, enccpaessize, enccpaes FROM encdata WHERE id='$senderid' AND enccpabename='$fname'";
    $result = query($sqlstr);
    list($enccpabename, $enccpabetype, $enccpabesize, $enccpabecontent, $enccpaesname, $enccpaestype, $enccpaessize, $enccpaescontent) 
		= mysql_fetch_array($result);

    if ($enccpabecontent && $enccpaescontent) 
    {
    	$fhandle = fopen($enccpabename, "w");
        fwrite($fhandle, $enccpabecontent);
        fclose($fhandle);
        
        $fhandle = fopen($enccpaesname, "w");
        fwrite($fhandle, $enccpaescontent);
        fclose($fhandle);
        
        return $enccpabename;
    }
    return false;
}

/* get PXK */
function getPXK($id) 
{
    $sqlstr = "SELECT pxkname, pxktype, pxksize, pxk FROM user WHERE id='$id'";
    $result = query($sqlstr);
    list($pxkname, $pxktype, $pxksize, $pxkcontent) = mysql_fetch_array($result);

    if ($pxkcontent) 
    {
    	$fhandle = fopen($pxkname, "w");
        fwrite($fhandle, $pxkcontent);
        fclose($fhandle);
    }
    return $pxkname;
}

/* get u_k received from this sender */
function getMyUK($myuid, $senderid) 
{
    $sqlstr = "SELECT id2uk FROM link WHERE id2='$myuid' AND id1='$senderid'";
    $result = query($sqlstr);
    list($my_uk) = mysql_fetch_array($result);
    return $my_uk;
}

/* get SK from senderid to myuid */
function getMySKFromThisContact($myuid, $senderid) 
{
    $sqlstr = "SELECT skname, sktype, sksize, sk FROM link WHERE id2='$myuid' AND id1='$senderid'";
    $result = query($sqlstr);
    list($skname, $sktype, $sksize, $skcontent) = mysql_fetch_array($result);

    if ($skcontent) 
    {
	    $fhandle = fopen($skname, "w");
        fwrite($fhandle, $skcontent);
        fclose($fhandle);
        return $skname;
    }
    return false;
}

/* get my delegated key */
function getMySKDel($delfromid, $delviaid, $myuid) 
{
    $sqlstr = "SELECT skdelname, skdel FROM delkey WHERE id1='$delfromid' AND id2='$delviaid' && id3='$myuid'";
    $result = query($sqlstr);
    list($skdelname, $skdelcontent) = mysql_fetch_array($result);

    if ($skdelcontent) 
    {
	    $fhandle = fopen($skdelname, "w");
        fwrite($fhandle, $skdelcontent);
        fclose($fhandle);
        return $skdelname;
    }
    return false;
}

/* print data in dataname */
function showData($datafilename) 
{
    $fhandle = fopen($datafilename, 'r');
    $datasize = filesize($datafilename);
    $datatype = filetype($datafilename);
    $datacontent = fread($fhandle, $datasize);

    fclose($fhandle);

    echo $datacontent;
    unlink($datafilename);
}

function dec_del($outfilename, $pkA, $pkB, $skdelBC, $lkB, $lkC, $pxinfileA, $pxinfileB)
{
	$del_dec_cmd = "cpabe-dec_delegated -o $outfilename $pkA $pkB $skdelBC $lkB $lkC $pxinfileA $pxinfileB";
	//echo $del_dec_cmd . "<br> === <br>";
	exec($del_dec_cmd);
	return file_exists($outfilename);
}
function convert($outname, $pkname, $enccpabename, $pxkname, $lkname, $uk)
{
	$cnv_cmd = "cpabe-convert -o $outname $pkname $enccpabename $pxkname $lkname $uk";
	exec($cnv_cmd);
	//echo $cnv_cmd . "<br> === <br>";
	return $outname;
}

/* decrypt with delegated key */
function conv_dec_delegated($delfromid, $delfromname, $delviaid, $filename)
{
	global $myuid;
	
	$enccpabenameA = getEnc($delfromid, $filename);

	//B converts using A's proxy
	$pkA = getPK($delfromid);
	$pxkA = getPXK($delfromid);
	$lkB = "lk-$delviaid";
	$ukB = getMyUK($delviaid, $delfromid);
	$outnameA = $enccpabenameA . ".proxy";
	convert($outnameA, $pkA, $enccpabenameA, $pxkA, $lkB, $ukB);
	
	//C converts using B's proxy
	$pkB = getPK($delviaid);
	$pxkB = getPXK($delviaid);
	$lkC = "lk-$myuid";
	$ukC = getMyUK($myuid, $delviaid);
	$outnameB = $delviaid . "-" . $enccpabenameA . ".proxy";
	convert($outnameB, $pkB, $enccpabenameA, $pxkB, $lkC, $ukC);
		
	$skdelBC = getMySKDel($delfromid, $delviaid, $myuid);
	$outfilename = substr($enccpabenameA, 0, -6);
	dec_del($outfilename, $pkA, $pkB, $skdelBC, $lkB, $lkC, $outnameA, $outnameB);
	
	if(file_exists($pkA))	unlink($pkA);
	if(file_exists($pxkA))	unlink($pxkA);
	if(file_exists($lkB))	unlink($lkB);
	if(file_exists($outnameA))	unlink($outnameA);
	
	if(file_exists($pkB))	unlink($pkB);
	if(file_exists($pxkB))	unlink($pxkB);
	if(file_exists($lkC))	unlink($lkC);
	if(file_exists($outnameB))	unlink($outnameB);
	
	if(file_exists($enccpabenameA)) unlink($enccpabenameA);
	$enccpaesname = substr($enccpabenameA, 0, -6) . ".cpaes";
	if(file_exists($enccpaesname)) unlink($enccpaesname);
	if(file_exists($skdelBC)) unlink($skdelBC);
	
	return $outfilename;
}

/* decrypt with regular key */
function convert_decrypt($fname) 
{
    global $myuid;
    global $cname;
    global $cid;

    $pkname = getPK($cid);
    $enccpabename = getEnc($cid, $fname);
    $enccpaesname = substr($enccpabename, 0, -6) . ".cpaes";
    
    $pxkname = getPXK($cid);

    $my_uk = getMyUK($myuid, $cid);
	$lk_name = "lk-$myuid";

    $cnv_cmd = "cpabe-convert $pkname $enccpabename $pxkname $lk_name $my_uk";
    exec($cnv_cmd);
    //echo $cnv_cmd . "=================================<br>";

    $cnv_encname = $enccpabename . ".proxy";
    $myskname = getMySKFromThisContact($myuid,$cid);
    $dec_cmd = "cpabe-dec $pkname $myskname $lk_name $cnv_encname";

    exec($dec_cmd);
    //echo $dec_cmd . "<br>";

    if (strlen($enccpabename) > 6) 
    {
        $dataname = substr($enccpabename, 0, -6); //fname -.cpabe
    }

    if ($pkname) 		unlink($pkname);
    if ($enccpabename)	unlink($enccpabename);
    if ($enccpaesname)	unlink($enccpaesname);
    if ($pxkname)		unlink($pxkname);
    if ($cnv_encname)	unlink($cnv_encname);
    if ($myskname)		unlink($myskname);
    if($lk_name)		unlink($lk_name);

  	return $dataname;
}

function showEncFiles() 
{
	if( (isset($_GET['cid']) && isset($_GET['cname'])) || (isset($_GET['delfromid']) && isset($_GET['delfromname']) && isset($_GET['delviaid'])) ) 
    {
        echo "<table width=\"600\" border=\"1\">";

        global $cid;
        global $cname;
        
        $del_key = 0;

		if(isset($_GET['cid']) && isset($_GET['cname']))
		{
			$cid = $_GET['cid'];
	        $cname = $_GET['cname'];
		}
		else if(isset($_GET['delfromid']) && isset($_GET['delfromname']) && isset($_GET['delviaid']) )
		{
			$del_key = 1;
			$cid = $_GET['delfromid'];
	        $cname = $_GET['delfromname'];
	        $delviaid = $_GET['delviaid'];
		}
       
        $query = "SELECT enccpabename, enccpabetype, enccpabesize, enccpabe FROM encdata WHERE id='$cid'";
        $result = query($query);
	 	
	 	echo "<tr>	<td> <b>Encrypted </b></td>	<td> <b>Plain Text </b></td> </tr>";
        
        while (list($name, $type, $size, $content) = mysql_fetch_array($result)) 
        {
			echo "<tr>";
			if($del_key == 1)
			{
				echo "<td> <a href=\"profile.php?delfromname=$cname&delfromid=$cid&delviaid=$delviaid&filename=$name&delegated=1\"> $name </a> <br> </td>	";
			}
			else
			{
				echo "<td> <a href=\"profile.php?cname=$cname&cid=$cid&filename=$name&delegated=0\"> $name </a> <br> </td>";
			}
			echo "<td>";
			if (isset($_GET['filename']) && isset($_GET['delegated']) && $_GET['filename'] == $name) 
			{
				if ($_GET['delegated'] == 0)
				{
               		$dataname = convert_decrypt($_GET['filename']);
					showData($dataname);
				}
				else //if($_GET['delegated'] == 1)
				{
					$delfromid = $_GET['delfromid'];
					$delfromname = $_GET['delfromname'];	
					$delviaid = $_GET['delviaid'];
					$filename = $_GET['filename'];				
					$dataname = conv_dec_delegated($delfromid, $delfromname, $delviaid, $filename);
					showData($dataname);
				}
				$_GET['filename'] = null;
				//$_GET['delegated'] = null;
           	}
			echo "</td>";
        	echo "</tr>";
        }
    	echo "</table>";
    }
}
?>
<html xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <title>Profile</title>
    </head>
    <body>
        <h4> 
        	<?php
				if (isset($_GET['cid']) && isset($_GET['cname'])) 
				{
    				$cname = $_GET['cname'];
    				echo $cname;
				} 
				else if (isset($_GET['delfromid']) && isset($_GET['delfromname']) && isset($_GET['delviaid'])) 
				{
				    $delfromname = $_GET['delfromname'];
				    echo $delfromname;
				} 
				else if (isset($_POST['dec_button'])) 
				{
    
				} 
				else 
				{
    				echo "No Keys Received!";
				}
			?>
		</h4>
        <form method="post" enctype="multipart/form-data" action="profile.php">
		<?php showEncFiles(); ?>

	  <table>
	    <tr>
	      <td> <input name="home_button" type="submit" class="box" id="home_button" value=" Home "> </td>
	    </tr>
	  </table>
        </form>

    </body>
</html>
