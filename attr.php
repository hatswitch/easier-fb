<?php
require_once 'facebook-php-sdk/src/facebook.php';
require_once 'dbappinclude.php';

if (isset($_POST['home_button'])) {
    header("Location: index.php");
}

if (isset($_POST['create_attr'])) {

    session_start();
    $facebook = unserialize($_SESSION['fb']);

    $attrs = null;
    $attrs = trim($_POST['attr_textarea']);


    if ($attrs) {
        $attrlist = explode(" ", $attrs);

        if ($facebook->getUser()) {
            try {
                $me = $facebook->api('/me');
                $uid = $me['id'];

                $sqlstr = "SELECT count(*) FROM user WHERE id='$uid'";
                list($result) = mysql_fetch_array(query($sqlstr));

                if ($result == 0) { //user does not exist
                    if (strlen($attrs) > 1000) {
                        $attrs = substr($attrs, 0, 1000);
                    }
                    $sqlstr = "INSERT INTO user (id,attr) VALUES('$uid', '$attrs')";
                } else {
                    $sqlstr = "SELECT attr from user WHERE id='$uid'";
                    list($result) = mysql_fetch_array(query($sqlstr));

		    if($result) {
		      $attrs = ($result . ' ' . $attrs);
		    }

                    if (strlen($attrs) > 1000) {
                        $attrs = substr($attrs, 0, 1000);
                    }

                    $attrs = trim($attrs);
                    $sqlstr = "UPDATE user SET attr='$attrs' WHERE id='$uid'";
                }
                query($sqlstr);
            } catch (FacebookApiException $e) {
                error_log($e);
            }
        }
    }
    header('Location: ' . $_SESSION['homepage']);
}

function showAttr() {

    echo '<b>Your defined attributes:' . '<br></b>';

    session_start();
    $facebook = unserialize($_SESSION['fb']);

    if ($facebook->getUser()) {
        try {
            $me = $facebook->api('/me');
            $uid = $me['id'];

            $sqlstr = "SELECT attr FROM user where id=$uid";
            $result = query($sqlstr);

            list($attrs) = mysql_fetch_array($result);

            if (strlen($attrs) > 0) {
                $attrlist = explode(" ", $attrs);

                $len = sizeof($attrlist);
                if ($len > 0) {
                    echo '<ul>';


                    for ($i = 0; $i < $len; $i++) {
                        echo '<li>' . $attrlist[$i];
                    }
                    echo '</ul>';
                }
            }
        } catch (FacebookApiException $e) {
            error_log($e);
        }
    }
}
?>

<html xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <title>Attributes</title>
    </head>
    <body>
<?php
if (isset($_POST['addattr'])) {
?>
        <h3>Create Attributes</h3>

        <form name="create_attr_form" method="post" enctype="multipart/form-data" action="attr.php"">
              <table width="600" border="0" cellpadding="1" cellspacing="1" class="box">
                <tr>
                    <td>
                        Enter Attributes (separated by space, max 1000 letters): <br>
                        <textarea cols="50" rows="10" name="attr_textarea"> </textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input name="create_attr" type="submit" class="box" id="create_attr" value=" Create ">
                    </td>
                </tr>
                <tr>
                    <td>
<?php
    showAttr();
?>
                    </td>
                </tr>
		<tr>
		    <td> <input name="home_button" type="submit" class="box" id="home_button" value=" Home "> </td>
		</tr>

            </table>
        </form>
<?php
}
?>

    </body>
</html>

