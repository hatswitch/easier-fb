<?php
require_once 'facebook-php-sdk/src/facebook.php';
require_once 'dbappinclude.php';

/* Home button pressed */
if (isset($_POST['home_button'])) {
   header("Location: index.php");
}

$facebook = null;
$me = null;

if (isset($_POST['rvk']) || isset($_POST['rvk_button'])) {

    session_start();
    $facebook = unserialize($_SESSION['fb']);

    if ($facebook->getUser()) {
        try {
            $me = $facebook->api('/me');
        } catch (FacebookApiException $e) {
            error_log($e);
        }
    }
    $myuid = $me['id'];
    if(isset($_POST['rvk_button']))
    {
        getPKMK($myuid);
    }
    $pxk_gen_cmd = "cpabe-revoke pk-$myuid mk-$myuid rvk-$myuid ";

    if (isset($_POST['contacts_checkbox'])) {

        $contacts_checkbox = $_POST['contacts_checkbox'];
        $id2uklist = null;
        $i=0;

        while (list ($key, $val) = @each($contacts_checkbox)) {
            $pxk_gen_cmd = $pxk_gen_cmd . " $val";
            $id2uklist[$i++] = $val;
        }

        //echo $pxk_gen_cmd;
        exec($pxk_gen_cmd);

        $pxkname = "rvk-$myuid";

        $fp = fopen($pxkname, 'r');
        $pxksize = filesize($pxkname);
        $pxktype = filetype($pxkname);
        $pxkcontent = fread($fp, $pxksize);
        $pxkcontent = addslashes($pxkcontent);
        fclose($fp);

        if (!get_magic_quotes_gpc()) {
            $pxkname = addslashes($pxkname);
        }

        $sqlstr = "UPDATE user SET pxk='$pxkcontent',pxkname='$pxkname',pxksize='$pxksize',pxktype='$pxktype' WHERE id=$myuid";
        query($sqlstr);

        $sqlstr = "UPDATE link SET revoked=1 WHERE id2uk=";

        for($i=0; $i<sizeof($id2uklist); $i++)
        {
            $sqlstr = $sqlstr. " '$id2uklist[$i]' ";

            if($i != (sizeof($id2uklist)-1))
            {
                $sqlstr = $sqlstr. " OR id2uk=";
            }
        }
        //echo $sqlstr;
        query($sqlstr);

        unlink("pk-$myuid");
        unlink("mk-$myuid");
        unlink($pxkname);
    }
}

function getPKMK($myuid) {
    $sqlstr = "SELECT pkname, pktype, pksize, pk, mkname, mktype, mksize, mk FROM user WHERE id='$myuid'";
    $result = query($sqlstr);

    list($pkname, $pktype, $pksize, $pkcontent, $mkname, $mktype, $mksize, $mkcontent) = mysql_fetch_array($result);

    //read pk and mk from database, generate key, save the keys, and delete pk, mk from disk
    if ($pkcontent && $mkcontent) {

        $fhandle = fopen($pkname, "w");
        fwrite($fhandle, $pkcontent);
        fclose($fhandle);

        $fhandle = fopen($mkname, "w");
        fwrite($fhandle, $mkcontent);
        fclose($fhandle);
    }
}

function showContacts() {
    global $facebook;
    global $me;

    if ($me) {
        $myuid = $me['id'];

        //$sqlstr = "SELECT id2 FROM fbfriend WHERE id1='$myuid'
        //            AND id2 IN (SELECT id FROM fbuser WHERE isappuser=1)"; //select FB friends who use this app
        $sqlstr = "SELECT fbuser.name,id2,id2uk FROM fbuser,link WHERE id1='$myuid' AND revoked='0' AND id2=fbuser.id";
        $result = query($sqlstr);

        while (list($name, $id, $iduk) = mysql_fetch_array($result)) {
            echo "<INPUT TYPE=CHECKBOX NAME=\"contacts_checkbox[]\" VALUE=\"$iduk\" >$name <br>";
        }
    }
}

function showSentKeys() {
    global $facebook;
    global $me;

    if ($me) {
        $myuid = $me['id'];

        $sqlstr = "SELECT fbuser.name, link.id2, attr, revoked, id2uk FROM fbuser,link WHERE link.id1=$myuid AND link.id2=fbuser.id";
        $result = query($sqlstr);

        //get the contactsc that I gave a key
        while (list($cname, $cid, $cattr, $crevoked, $cuk) = mysql_fetch_array($result)) {
            echo "<tr> <td>$cname </td> <td> $cattr </td> <td> $crevoked  </td></tr>";
        }
    }
}

function showReceivedKeys() {
    global $facebook;
    global $me;

    if ($me) {
        $myuid = $me['id'];

        $sqlstr = "SELECT fbuser.name, link.id1, link.sksize, link.sktype, link.skname, link.sk FROM fbuser,link
            WHERE link.id2='$myuid' AND link.id1=fbuser.id";
        $result = query($sqlstr);

        //get the contactsc that I gave a key
        while (list($cname, $cid, $csksize, $csktype, $cskname) = mysql_fetch_array($result)) {
            echo "<tr> <td>$cname </td> <td> <a href=\"download.php?filename=<?php echo $cskname; ?>&type=2\"> Secret Key </a> </td></tr>";
        }
    }
}
?>

<html xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <title>Revoke</title>
    </head>
    <body>
        <h3>Revoke Keys</h3>

        <form method="post" enctype="multipart/form-data" action="rvk.php">
            <table width="700" border="1" cellpadding="1" cellspacing="1" class="box">
                <tr>
                    <td>
                        <b>Select contacts: </b><br>
<?php showContacts(); ?>

                    </td>
                </tr>
                <tr>
                    <td><input name="rvk_button" type="submit" class="box" id="rvk_button" value=" Revoke Key "></td>
                </tr>
		<tr>
	      		<td> <input name="home_button" type="submit" class="box" id="home_button" value=" Home "> </td>
	    	</tr>

                <tr>
                    <td>
                        <b>My assigned keys:</b> <br>
                        <table width ="700" border="1">

                            <tr>
                                <td>
                                    <b> To</b>
                                </td>
                                <td>
                                    <b>Attributes</b>
                                </td>
                                <td>
                                    <b>Revoked </b>
                                </td>
                            </tr>
<?php showSentKeys(); ?>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>My received keys:</b> <br>
                        <table width ="700" border="1">

                            <tr>
                                <td>
                                    <b> From </b>
                                </td>
                                <td>
                                    <b>Key</b>
                                </td>

                            </tr>
<?php showReceivedKeys(); ?>
                        </table>

                    </td>
                </tr>

            </table>

        </form>

    </body>
</html>
