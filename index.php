<?php
require_once 'facebook-php-sdk/src/facebook.php';
require_once 'dbappinclude.php';
require_once 'appinclude.php';

$_SESSION['homepage'] = $_SERVER['PHP_SELF'];

$me = null;
// Session based API call.
//if ($session) {
//    try {
//        $access_token = $session['access_token'];
//        $me = $facebook->api('/me?access_token=' . $access_token);
//    } catch (FacebookApiException $e) {
//        error_log($e);
//    }
//}

$user = $facebook->getUser();

if ($user) {
    try
    {
      $me = $facebook->api('/me');
    } 
    catch (FacebookApiException $e) 
    {
      error_log($e);
      $user = null;
    }
    $logoutUrl = $facebook->getLogoutUrl();
} else {
    $loginUrl = $facebook->getLoginUrl();
}

function fieldExists($fieldType) {
    global $me;
    $myuid = $me['id'];
    $sqlstr = null;
    $result = null;
    //check setup
    if ($fieldType == 1) {
        $sqlstr = "SELECT pk,mk FROM user WHERE id='$myuid'";
        $result = query($sqlstr);
        list($pk, $mk) = mysql_fetch_array($result);

        if ($pk && $mk)
            return true;
    }

    //check proxy key
    else if ($fieldType == 2) {
        $sqlstr = "SELECT pxk FROM user WHERE id='$myuid'";
        $result = query($sqlstr);
        list($pxk) = mysql_fetch_array($result);

        if ($pxk)
            return true;
    }

    return false;
}

function showReceivedKeyContacts() {
    global $facebook;
    global $me;

    if ($me) {
        $myuid = $me['id'];

        $sqlstr = "SELECT fbuser.name, link.id1 FROM fbuser,link
            WHERE link.id2='$myuid' AND link.id1=fbuser.id";
        $result = query($sqlstr);

        while (list($cname, $cid) = mysql_fetch_array($result)) {
?>
            <tr>
                <td> <a href="profile.php?cname=<?php echo $cname; ?>&cid=<?php echo $cid; ?>"> <?php echo $cname; ?> </a> <br> </td>
            </tr>
<?php
        }
    }
}

function showFoF()
{
    global $facebook;
    global $me;

    if ($me) {
        $myuid = $me['id'];

        $sqlstr = "SELECT delkey.id1,fbuser.name, delkey.id2 FROM delkey,fbuser WHERE delkey.id3 LIKE '$myuid' AND fbuser.id LIKE delkey.id1";
        $result = query($sqlstr);

        while (list($delfromid,$delfromname,$delviaid) = mysql_fetch_array($result)) {
?>
            <tr>
                <td> <a href="profile.php?delfromid=<?php echo $delfromid; ?>&delfromname=<?php echo $delfromname; ?>&delviaid=<?php echo $delviaid; ?>"> <?php echo $delfromname; ?> </a> <br> </td>
            </tr>
<?php
        }
    }
	
}

function addFBFriends($myuid, $frnd) {
    $sqlstr = "INSERT INTO fbfriend (id1, id2) VALUES";

    for ($i = 0; $i < sizeof($frnd['data']); $i++) {
        $friendid = $frnd['data'][$i]['id'];
        $sqlstr = $sqlstr . "('$myuid', '$friendid')";

        if ($i != (sizeof($frnd['data']) - 1)) {
            $sqlstr = $sqlstr . ',';
        }
    }

    if(sizeof($sqlstr) > sizeof("INSERT INTO fbfriend (id1, id2) VALUES"))
      query($sqlstr);
}

function fetchFriends() {
    $facebook = unserialize($_SESSION['fb']);
    //if ($facebook->getSession()) {
    if ($facebook->getUser()) {
        try {
            $me = $facebook->api('/me');
            $myuid = $me['id'];
            $myname = $me['name'];

            $sqlstr = "SELECT id, isappuser FROM fbuser where id=$myuid";
            $result = query($sqlstr);
            if (mysql_num_rows($result) > 0) {
                list($id, $isappuser) = mysql_fetch_array($result);

                if ($isappuser == 0) {
                    $sqlstr = "UPDATE fbuser SET isappuser=1 WHERE id=$myuid"; // if I am not in fbuser, or not appuser, add me.
                    query($sqlstr);
                }
            } else { //if my first time, add my friends to the database
                $myname2 = addslashes($myname); // replace ' with \'
                //$about = $me['about'];
		$about = $me['bio'];                
		$gender = $me['gender'];

                $sqlstr = "INSERT INTO fbuser (id, name, isappuser, about, gender) VALUES('$myuid', '$myname2', 1, '$about', '$gender')"; // if I am not in fbuser, or not appuser, add me.
                query($sqlstr);

                $frnd = $facebook->api('/me/friends');
                
                if($frnd)
                {
                for ($i = 0; $i < sizeof($frnd['data']); $i++) {
                    $friendid = $frnd['data'][$i]['id'];
                    $friendname = $frnd['data'][$i]['name'];
                    //echo $friendname . " -- ";
                    $sqlstr = "SELECT * FROM fbuser where id=$friendid";
                    $result = query($sqlstr);
                    if (mysql_num_rows($result) == 0) {
                        $friendname2 = addslashes($friendname); // replace ' by \'
                        $sqlstr = "INSERT INTO fbuser (id, name, isappuser) VALUES('$friendid', '$friendname2', 0)"; // if friend is not in fbuser, add.
                        query($sqlstr);
                    }
                }
                addFBFriends($myuid, $frnd);
                }
            }
        } catch (FacebookApiException $e) {
            error_log($e);
        }
    }
}

function showAttr() {

    echo '<b>Your defined attributes:' . '<br></b>';

    $facebook = unserialize($_SESSION['fb']);

    //if ($facebook->getSession()) {
    if ($facebook->getUser()) {
        try {
            $me = $facebook->api('/me');
            $uid = $me['id'];

            $sqlstr = "SELECT attr FROM user where id='$uid'";
            $result = query($sqlstr);

            list($attrs) = mysql_fetch_array($result);

            if (strlen($attrs) > 0) {
                $attrlist = explode(" ", $attrs);

                $len = sizeof($attrlist);
                if ($len > 0) {
                    echo '<ul>';


                    for ($i = 0; $i < $len; $i++) {
                        echo '<li>' . $attrlist[$i];
                    }
                    echo '</ul>';
                }
            }
        } catch (FacebookApiException $e) {
            error_log($e);
        }
    }
}

function showEncFiles() {

    global $me;

    $myuid = $me['id'];

    $query = "SELECT enccpabename FROM encdata WHERE id='$myuid'";

    $result = query($query);

    if ($result) {
?>
        <br><br>
        <b>Files You Have Encrypted:</b>
        <table width="700" border="1" >
<?php
    }

    while (list($name) = mysql_fetch_array($result)) {
?>
            <tr> 
				<!-- <td> <a href="download.php?filename=<?php echo $name; ?>&type=3"> <?php echo $name ?> </a></td> -->
				<td> <?php echo $name ?> </td>
            </tr>
    <?php
    }
    if ($result) {
    ?>
    </table>
<?php
    }
}
?>
<!doctype html>
<html xmlns:fb="http://www.facebook.com/2008/fbml">
    <body>
        <!--
          We use the JS SDK to provide a richer user experience. For more info,
          look here: http://github.com/facebook/connect-js
        -->
      <div id="allContent" style="height:100%">
	<div id="output" style="color: #FFFFFF;" />
      </div>

        <div id="fb-root"></div>
        <script>
            window.fbAsyncInit = function() {
                FB.init({
			appId   : '<?php echo $facebook->getAppId(); ?>',
                        //session : <?php echo json_encode($session); ?>, // don't refetch the session when PHP already has it
			oauth	: true,
                        status  : true, // check login status
                        cookie  : true, // enable cookies to allow the server to access the session
                        xfbml   : true // parse XFBML
                    });

                    // whenever the user logs in, we refresh the page
                    FB.Event.subscribe('auth.login', function() {
                        window.location.reload();
                    });
                };

                (function() {
                    var e = document.createElement('script');
                    e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
                    e.async = true;
                    document.getElementById('fb-root').appendChild(e);
                }());
            </script>
<?php if ($me): ?>

                <a href="<?php echo $logoutUrl; ?>">
                    <img src="http://static.ak.fbcdn.net/rsrc.php/z2Y31/hash/cxrz4k7j.gif">
                </a>
<?php else: ?>
                    <div>
                        Using JavaScript &amp; XFBML: <fb:login-button></fb:login-button>
                    </div>
<?php endif ?>
            <table  width="700 "border="1" cellpadding="1" cellspacing="1" class="box">
                <tr>
                    <td>
                        <table width="350" border="1">
                            <form name="setupform" method="post" enctype="multipart/form-data" action="setup.php">
                                <tr>
                                    <td>
                                        <!--
                                        <b>Please select the max number of revocations:<br></b>
                                        <select name="attr">
                                            <option value="10" selected> 10 </option>
                                            <option value="50"> 50 </option>
                                            <option value="100"> 100</option>
                                            <option value="500"> 500 </option>
                                        </select>
                                        -->
                                        <input name="setup" type="submit" class="box" id="setup" value=" Setup "
                                               onclick=<?php
            if (fieldExists(1) == true) {
                echo "\"alert('You have Setup already!'); return false\"";
            }
?>>
                                        <font size="1" > [Default max revoked users is set to 100] </font>

                                    </td>
                                </tr>
                            </form>
                            <form method="post" enctype="multipart/form-data" action="proxyrekey.php">
                                <tr>
                                    <td>
                                        <input name="keyproxy" type="submit" class="box" id="keyproxy" value=" Key Proxy "
                                               onclick=<?php
            if (fieldExists(1) == false) {
                echo "\"alert('Please Setup First!'); return false\"";
            }
            if (fieldExists(2) == true) {
                echo "\"alert('You have Setup Proxy Key already!'); return false\"";
            }
?>>
                                </td>
                            </tr>
                        </form>
                        <form method="post" enctype="multipart/form-data" action="attr.php">
                            <tr>
                                <td>
                                    <input name="addattr" type="submit" class="box" id="addattr" value=" Add Attributes ">
                                </td>
                            </tr>
                        </form>

                        <form name="keygenform" method="post" enctype="multipart/form-data" action="keygen.php">
                            <tr>
                                <td>
                                    <input name="keyGen" type="submit" class="box" id="keyGen" value=" KeyGen "
                                           onclick=<?php
            if (fieldExists(1) == false) {
                echo "\"alert('Please Setup First!'); return false\"";
            }
?> >
                                </td>
                            </tr>
                        </form>

			<form name="delkeyform" method="post" enctype="multipart/form-data" action="delkey.php">
                            <tr>
                                <td>
                                    <input name="delKey" type="submit" class="box" id="delKey" value=" Delegate"
                                           onclick=<?php
            if (fieldExists(1) == false) {
                echo "\"alert('Please Setup First!'); return false\"";
            }
?>>
                                </td>
                            </tr>
                        </form>
                        <form method="post" enctype="multipart/form-data" action="enc.php">
                            <tr>
                                <td>
                                    <input name="enc" type="submit" class="box" id="enc" value=" Encrypt "
                                           onclick=<?php
            if (fieldExists(1) == false) {
                echo "\"alert('Please Setup First!'); return false\"";
            }
?> >
                                </td>
                            </tr>
                        </form>

                        <form method="post" enctype="multipart/form-data" action="rvk.php">
                            <tr>
                                <td>
                                    <input name="rvk" type="submit" class="box" id="rvk" value=" Revoke ">
                                </td>
                            </tr>
                        </form>
                    </table>
                </td>

                <td style="vertical-align: top;">
                    <table width="350" border="1">
                        <tr>
                            <td>
<?php
            $query = "SELECT pkname, mkname FROM user where id='" . $me['id'] . "'";
            $result = query($query);

            list($pkname, $mkname) = mysql_fetch_array($result);
            if ($pkname && $mkname) {
                $_SESSION['setup'] = true;
?>

                                <a href="download.php?filename=<?php echo $pkname; ?>&type=1"> Public Key </a> <br>
                                <a href="download.php?filename=<?php echo $mkname; ?>&type=0"> Master Secret Key </a> <br>

                                <?php
                            } else {
                                $_SESSION['setup'] = false;
                            }

                            $query = "SELECT pxkname FROM user where id='" . $me['id'] . "'";
                            $result = query($query);

                            list($pxkname) = mysql_fetch_array($result);

                            if ($pxkname) {
                                ?>
                                <a href="download.php?filename=<?php echo $pxkname; ?>&type=4"> Proxy Key </a> <br>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr border="1">
                            <td>
                                <?php showAttr(); ?>
<?php fetchFriends(); ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <br><br>

        <b>Your Contacts:</b><br>
        <table width="700" border="1">
	  <?php showReceivedKeyContacts(); ?>
        </table>
	<br><br>
	<b>Friends of Friends: </b><br>
        <table width="700" border="1">
	  <?php showFoF(); ?>
        </table>
<?php showEncFiles(); ?>
    </body>
</html>
